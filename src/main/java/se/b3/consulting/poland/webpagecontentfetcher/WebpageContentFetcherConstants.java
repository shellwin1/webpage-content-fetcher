package se.b3.consulting.poland.webpagecontentfetcher;

import lombok.experimental.UtilityClass;

/**
 * Utility class with Content Fetcher constants.
 *
 * @author Łukasz Skorupka
 * @since 0.1.0
 */
@UtilityClass
public class WebpageContentFetcherConstants {
    public static final String FETCH_CONTENT_PATH = "/fetchContent";
}
