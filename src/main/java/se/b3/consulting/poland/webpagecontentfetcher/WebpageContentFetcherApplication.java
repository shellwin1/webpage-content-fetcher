package se.b3.consulting.poland.webpagecontentfetcher;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebpageContentFetcherApplication {
    public static void main(String[] args) {
        SpringApplication.run(WebpageContentFetcherApplication.class, args);
    }
}
