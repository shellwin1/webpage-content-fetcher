package se.b3.consulting.poland.webpagecontentfetcher.persistence;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository with operations on {@link Content}.
 *
 * @author Łukasz Skorupka
 * @version 0.1.0
 */
@Repository
public interface ContentRepository extends ReactiveMongoRepository<Content, ObjectId> {
}
