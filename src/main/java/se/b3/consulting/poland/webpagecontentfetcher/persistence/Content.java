package se.b3.consulting.poland.webpagecontentfetcher.persistence;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Builder;
import lombok.Data;
import org.bson.types.Binary;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.time.LocalDateTime;


/**
 * @author Łukasz Skorupka
 * @since 0.1.0
 */
@Builder
@Data
@Document
public class Content {

    @Id
    private ObjectId id;

    @Field
    @CreatedDate
    private LocalDateTime initDate;

    @Field
    @LastModifiedDate
    private LocalDateTime fetchedDate;

    @Field
    private final String url;

    @Field
    private final Binary content;

    @Field
    private final String name;

    @Field
    private final String contentType;
}
