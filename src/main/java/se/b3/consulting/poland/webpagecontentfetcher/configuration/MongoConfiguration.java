package se.b3.consulting.poland.webpagecontentfetcher.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.EnableReactiveMongoAuditing;

/**
 * Configuration of persistence functionality.
 *
 * @author Łukasz Skorupka
 * @since 0.1.0
 */
@Configuration
@EnableReactiveMongoAuditing
public class MongoConfiguration {
}