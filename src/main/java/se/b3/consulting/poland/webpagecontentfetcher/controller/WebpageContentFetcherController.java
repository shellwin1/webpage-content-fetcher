package se.b3.consulting.poland.webpagecontentfetcher.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;
import se.b3.consulting.poland.webpagecontentfetcher.dto.ContentDTO;
import se.b3.consulting.poland.webpagecontentfetcher.service.ContentFetcherService;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static se.b3.consulting.poland.webpagecontentfetcher.WebpageContentFetcherConstants.FETCH_CONTENT_PATH;

/**
 * Controller with API for fetching content from website.
 *
 * @author Łukasz Skorupka
 * @since 0.1.0
 */
@RestController
@RequiredArgsConstructor
public class WebpageContentFetcherController {

    private final ContentFetcherService contentFetcherService;

    @Operation(summary = "Runs asynchronous fetching content from webpage",
            responses = @ApiResponse(responseCode = "201", content = @Content(examples = @ExampleObject(value = "{\n" +
                    "   \"id\":\"635e652423c86b2bc9bbaea0\"\n" +
                    "}"))))
    @GetMapping(path = FETCH_CONTENT_PATH, produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public Mono<ContentDTO> fetchContent(@RequestParam final String url) {
        return contentFetcherService.fetchContent(url);
    }
}
