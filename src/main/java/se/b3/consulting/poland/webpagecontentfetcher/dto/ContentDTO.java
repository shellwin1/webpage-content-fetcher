package se.b3.consulting.poland.webpagecontentfetcher.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.bson.types.ObjectId;
import se.b3.consulting.poland.webpagecontentfetcher.persistence.Content;

/**
 * Represents initialized fetching @{@link Content}
 *
 * @author Łukasz Skorupka
 * @version 0.1.0
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@ToString
@EqualsAndHashCode
public class ContentDTO {

    @JsonSerialize(using = ToStringSerializer.class)
    private ObjectId id;
}
