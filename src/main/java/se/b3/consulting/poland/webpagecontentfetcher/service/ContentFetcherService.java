package se.b3.consulting.poland.webpagecontentfetcher.service;

import lombok.RequiredArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import se.b3.consulting.poland.webpagecontentfetcher.dto.ContentDTO;

/**
 * @author Łukasz Skorupka
 * @since 0.1.0
 */
@Service
@RequiredArgsConstructor
public class ContentFetcherService {

    private final AsyncContentFetcherService asyncContentFetcherService;
    private final FetchingInitializerService fetchingInitializerService;

    public Mono<ContentDTO> fetchContent(final String url) {
        return fetchingInitializerService.initialize(url).map(data -> {
            asyncContentFetcherService.fetchContent(url, data);
            return data;
        });
    }
}
