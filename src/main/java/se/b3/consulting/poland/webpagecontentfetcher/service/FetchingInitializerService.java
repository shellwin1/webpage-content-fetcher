package se.b3.consulting.poland.webpagecontentfetcher.service;

import lombok.RequiredArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import se.b3.consulting.poland.webpagecontentfetcher.dto.ContentDTO;
import se.b3.consulting.poland.webpagecontentfetcher.persistence.Content;
import se.b3.consulting.poland.webpagecontentfetcher.persistence.ContentRepository;

/**
 * @author Łukasz Skorupka
 * @since 0.1.0
 */
@Service
@RequiredArgsConstructor
public class FetchingInitializerService {

    private final ContentRepository contentRepository;

    public Mono<ContentDTO> initialize(final String url) {
        Content content = Content.builder()
                .url(url)
                .build();

        return contentRepository.save(content).map(data -> new ContentDTO(data.getId()));
    }
}
