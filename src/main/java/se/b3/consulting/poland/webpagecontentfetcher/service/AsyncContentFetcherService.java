package se.b3.consulting.poland.webpagecontentfetcher.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.bson.types.Binary;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.MimeType;
import org.springframework.web.reactive.function.client.ExchangeStrategies;
import org.springframework.web.reactive.function.client.WebClient;
import se.b3.consulting.poland.webpagecontentfetcher.dto.ContentDTO;
import se.b3.consulting.poland.webpagecontentfetcher.persistence.Content;
import se.b3.consulting.poland.webpagecontentfetcher.persistence.ContentRepository;

/**
 * @author Łukasz Skorupka
 * @since 0.1.0
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class AsyncContentFetcherService {

    private final ContentRepository contentRepository;

    @Async("asyncThreadPool")
    public void fetchContent(final String url, ContentDTO initContent) {
        final WebClient webClient = WebClient.builder()
                .baseUrl(url)
                .exchangeStrategies(ExchangeStrategies.builder()
                        .codecs(configurer -> configurer.defaultCodecs().maxInMemorySize(300 * 1024))
                        .build())
                .build();
        webClient.get()
                .exchangeToMono(response -> response.bodyToMono(byte[].class)
                        .map(Binary::new)
                        .map(binary -> Content.builder()
                                .id(initContent.getId())
                                .url(url)
                                .contentType(response.headers().contentType().map(MimeType::toString).orElse(null))
                                .content(binary)
                                .build()))
                .subscribe(data -> contentRepository.save(data).subscribe());
    }
}
