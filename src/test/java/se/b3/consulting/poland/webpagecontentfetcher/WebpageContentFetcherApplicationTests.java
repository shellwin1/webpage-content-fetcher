package se.b3.consulting.poland.webpagecontentfetcher;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.testcontainers.containers.MongoDBContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import se.b3.consulting.poland.webpagecontentfetcher.dto.ContentDTO;
import se.b3.consulting.poland.webpagecontentfetcher.persistence.Content;
import se.b3.consulting.poland.webpagecontentfetcher.persistence.ContentRepository;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static se.b3.consulting.poland.webpagecontentfetcher.WebpageContentFetcherConstants.FETCH_CONTENT_PATH;

@SpringBootTest
@Testcontainers
@AutoConfigureWebTestClient
class WebpageContentFetcherApplicationTests {

    // Image from https://www.b3.se/pl
    private static final String TEST_FILE_WEBPAGE_URL = "https://images.ctfassets.net/edp4l8jwwyll/1s50QKCLZkKB7TsGtg78W6/c28615a05530147382d652af75028bc5/100.jpg?w=1848&fm=webp&q=70";
    // Static HTML Content
    private static final String TEST_FILE_PATH = "src/test/resources/100.webp";
    private static final String TEST_JSON_PATH = "src/test/resources/page-data.json";
    private static final String TEST_HTML_WEBPAGE_URL = "https://www.b3.se/pl";
    private static final String TEST_JSON_WEBPAGE_URL = "https://www.b3.se/page-data/case/polska/page-data.json";
    private static final String MONGO_DOCKER_IMAGE_NAME = "mongo:5.0.13";
    @Autowired
    private WebTestClient webTestClient;

    @Autowired
    private ThreadPoolTaskExecutor asyncThreadPool;

    @Autowired
    private ContentRepository contentRepository;

    @Container
    private static final MongoDBContainer MONGO_DB_CONTAINER = new MongoDBContainer(MONGO_DOCKER_IMAGE_NAME);

    @DynamicPropertySource
    static void setUpMongoUri(final DynamicPropertyRegistry registry) {
        registry.add("spring.data.mongodb.uri", MONGO_DB_CONTAINER::getReplicaSetUrl);
    }


    @BeforeEach
    void cleanUp() {
        this.contentRepository.deleteAll()
                .subscribe();
    }

    @Test
    void shouldReturnObjectIdAndStatusCodeCreated() {
        this.webTestClient
                .get()
                .uri(uriBuilder -> uriBuilder.path(FETCH_CONTENT_PATH)
                        .queryParam("url", TEST_FILE_WEBPAGE_URL)
                        .build())
                .exchange()
                .expectStatus().isCreated()
                .expectBody(ContentDTO.class);
    }

    @Test
    void shouldFetchContentAndSaveInDbWhenIsFile() throws InterruptedException, IOException {
        ContentDTO contentDTO = this.webTestClient
                .get()
                .uri(uriBuilder -> uriBuilder.path(FETCH_CONTENT_PATH)
                        .queryParam("url", TEST_FILE_WEBPAGE_URL)
                        .build())
                .exchange()
                .expectStatus().isCreated()
                .returnResult(ContentDTO.class)
                .getResponseBody()
                .blockFirst();

        boolean awaitTermination = asyncThreadPool.getThreadPoolExecutor().awaitTermination(1, TimeUnit.SECONDS);
        assertThat(awaitTermination, equalTo(false));

        assert contentDTO != null;
        Content content = contentRepository.findById(contentDTO.getId()).block();
        assert content != null;
        assertThat(contentDTO.getId(), equalTo(content.getId()));
        assertThat(content.getContentType(), equalTo("image/webp"));
        assertThat(content.getFetchedDate(), notNullValue());
        assertThat(content.getContent(), notNullValue());

        byte[] fileBytes = Files.readAllBytes(Paths.get(TEST_FILE_PATH));
        assertThat(content.getContent().getData(), equalTo(fileBytes));
    }


    @Test
    void shouldFetchContentAndSaveInDbWhenWebpageIsHTML() throws InterruptedException, IOException {
        ContentDTO contentDTO = this.webTestClient
                .get()
                .uri(uriBuilder -> uriBuilder.path(FETCH_CONTENT_PATH)
                        .queryParam("url", TEST_HTML_WEBPAGE_URL)
                        .build())
                .exchange()
                .expectStatus().isCreated()
                .returnResult(ContentDTO.class)
                .getResponseBody()
                .blockFirst();

        boolean awaitTermination = asyncThreadPool.getThreadPoolExecutor().awaitTermination(1, TimeUnit.SECONDS);
        assertThat(awaitTermination, equalTo(false));

        assert contentDTO != null;
        Content content = contentRepository.findById(contentDTO.getId()).block();
        assert content != null;
        assertThat(contentDTO.getId(), equalTo(content.getId()));
        assertThat(content.getContentType(), equalTo("text/html;charset=utf-8"));
        assertThat(content.getFetchedDate(), notNullValue());
        assertThat(content.getContent(), notNullValue());

        // Regex for HTML Format
        Pattern htmlPattern = Pattern.compile(".*\\<[^>]+>.*", Pattern.DOTALL);
        boolean isHTML = htmlPattern.matcher(new String(content.getContent().getData(), StandardCharsets.UTF_8)).matches();
        assertThat(isHTML, equalTo(true));
    }

    @Test
    void shouldFetchContentAndSaveInDbWhenWebpageIsJSON() throws InterruptedException, IOException {
        ContentDTO contentDTO = this.webTestClient
                .get()
                .uri(uriBuilder -> uriBuilder.path(FETCH_CONTENT_PATH)
                        .queryParam("url", TEST_JSON_WEBPAGE_URL)
                        .build())
                .exchange()
                .expectStatus().isCreated()
                .returnResult(ContentDTO.class)
                .getResponseBody()
                .blockFirst();

        boolean awaitTermination = asyncThreadPool.getThreadPoolExecutor().awaitTermination(1, TimeUnit.SECONDS);
        assertThat(awaitTermination, equalTo(false));

        assert contentDTO != null;
        Content content = contentRepository.findById(contentDTO.getId()).block();
        assert content != null;
        assertThat(contentDTO.getId(), equalTo(content.getId()));
        assertThat(content.getContentType(), equalTo("application/json;charset=utf-8"));
        assertThat(content.getFetchedDate(), notNullValue());
        assertThat(content.getContent(), notNullValue());

        byte[] jsonBytes = Files.readAllBytes(Paths.get(TEST_JSON_PATH));
        assertThat(jsonBytes, equalTo(content.getContent().getData()));
    }
}
