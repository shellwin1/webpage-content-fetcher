# Introduction
This is a POC of the project to serve a single point of fetching content from webpage

# Documentation
Swagger: /swagger
API Docs: /api-docs

# Dependencies
In order to run requires:
* MongoDB or compatible database as a backend
* Java 11 and Maven

# Not implemented features
* 'Task' entity collects information about task id, status and fetched content.
* External service and queue to asynchronous fetching data
* Unit tests
* Versioning of content webpage in Content 'Entity'
* Encrypt credentials properties for Mongo DB Connection.
* JavaDoc for methods.
* Implement Global Error Handler for incorrect requests.